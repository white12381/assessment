import SummaryCard from "../components/dashboard/summaryCard";
import Iphone13 from "../assets/Images/Iphone13.png";
import Iphone13Black from "../assets/Images/Iphone13Black.png";
import Graph from "../assets/Images/Graph.png";
import User from "../assets/Images/User.png";
import Bag from "../assets/Images/Bag.png";
import FolderWhite from "../assets/Images/FolderWhite.png";
import Cart from "../assets/Images/Cart.png";
import DurationDropDown from "../components/dashboard/durationDropDown";
import { VictoryBar, VictoryChart, VictoryPie } from "victory";
import OrderSummary from "../components/dashboard/orderSummary";
const DashBoard = () => {
  const content1 = [
    {
      title: "Sales",
      price: "₦4,000,000.00",
    },
    {
      title: "Volume",
      price: "450",
      discount: "+20.00%",
    },
  ];
  const content2 = [
    {
      title: "Customers",
      price: "1,250",
      discount: "+15.80%",
    },
    {
      title: "Active",
      price: "1,180",
      discount: "85%",
    },
  ];
  const content3 = [
    {
      title: "All Orders",
      price: "450",
    },
    {
      title: "Pending",
      price: "5",
    },
    {
      title: "Completed",
      price: "445",
    },
  ];
  const content4 = [
    {
      title: "All Products",
      price: "45",
    },
    {
      title: "Active",
      price: "32",
      discount: "+24%",
    },
  ];
  const content5 = [
    {
      title: "Abandoned Cart",
      price: "20%",
      discount: "+0.00%",
    },
    {
      title: "Customers",
      price: "30",
    },
  ];
  return (
    <div>
      <div className="flex flex-col md:flex-row gap-[10px] w-full mb-4">
        <SummaryCard customStyle={"w-full md:w-4/12"} image={Graph} contents={content1} />
        <SummaryCard
          customStyle={"w-full md:w-4/12"}
          image={User}
          imageColor={"!bg-customYellow4"}
          contents={content2}
        />
        <SummaryCard
          image={Bag}
          customStyle={"w-full md:w-5/12"}
          imageColor={"!bg-customYellow4"}
          contents={content3}
        />
      </div>
      <div className="flex flex-col md:flex-row  gap-[10px]">
        <div className="w-full md:w-3/5 grid md:grid-cols-2 gap-[10px] col-span-2">
          <div className="bg-white   rounded-xl py-[21px]  px-[20px]">
            <main className="flex justify-between">
              {" "}
              <h3 className="font-medium font-inter text-base">Marketting</h3>
              <DurationDropDown />
            </main>
            <div className="flex justify-between my-4">
              <article>
                <div className="bg-customBlue marketingCircle"></div>
                <h4 className="marketingText">Acquisition</h4>
              </article>
              <article>
                <div className="bg-customBlue3 marketingCircle"></div>
                <h4 className="marketingText">Purchase</h4>
              </article>{" "}
              <article>
                <div className="bg-customYellow2 marketingCircle"></div>
                <h4 className="marketingText">Retention</h4>
              </article>
            </div>
            <main className="bg-customGray4 rounded-full h-[205px] w-[205px] mx-auto">
              <VictoryPie
                style={{ labels: { fill: "white" } }}
                colorScale={["#97A5EB", "#5570F1", "#FFCC91"]}
                innerRadius={100}
                data={[
                  { x: " ", y: 200 },
                  { x: " ", y: 600 },
                  { x: " ", y: 400 },
                ]}
              />
            </main>
          </div>
          <div className="grid content-between gap-y-10">
            <SummaryCard
              titleStyle={"!text-white"}
              priceStyle={"!text-white"}
              imageColor={"bg-white/30"}
              discountStyle={"!text-customHash mx-2"}
              hideDuration={true}
              image={FolderWhite}
              contents={content4}
              customStyle={"!bg-customBlue "}
            />
            <SummaryCard
              titleStyle={"text-customRed"}
              priceStyle={"!text-customBlack"}
              imageColor={"bg-customYellow4"}
              image={Cart}
              contents={content5}
            />
          </div>
          <div className="bg-white rounded-xl py-[15px] px-[20px] mt-5 col-span-2">
            <main className="flex items-center justify-between gap-x-4">
              <div className="flex items-center gap-x-4">
              <h4 className="text-customBlack font-medium text-base">
                Summary
              </h4>
              <DurationDropDown
                buttonText={"Sales"} dropDownStyle={'!text-customBlue4'}
                buttonStyle={
                  "bg-customGray4 !text-customBlue4 font-inter text-sm p-2 rounded-lg px-4" 
                }
              />
              </div>
              <DurationDropDown
                buttonText={"Last 7 Days"} dropDownStyle={'!text-customBlack2'}
                buttonStyle={
                  "!text-customBlack2 font-inter text-xs p-2 rounded-lg px-4" 
                }
              />
            </main>
            <VictoryChart 
  domainPadding={20}    domain={{  y: [0, 100] }}  
>
  <VictoryBar alignment="start"  cornerRadius={5}
    style={{ data: { fill: "#5570F1", width: 13, borderRadius: '50px'} }}
    data={[
      {
        x: 'Sept 10', y: 90
      },
      {
        x: 'Sept 11', y: 30
      },{
        x: 'Sept 12', y: 60
      },{
        x: 'Sept 13', y: 20
      }
    ]}
  />
</VictoryChart>
          </div>
        </div>
        <div className="w-full md:w-2/5 bg-white rounded-xl p-5">
          <h4 className="font-inter text-base text-customBlack font-medium mb-5">
            Recent Orders
          </h4>
          <div className="space-y-6">
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13}
              date={"12 Sept 2022"}
              status={"Pending"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13Black}
              date={"12 Sept 2022"}
              status={"Completed"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13}
              date={"12 Sept 2022"}
              status={"Pending"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13Black}
              date={"12 Sept 2022"}
              status={"Completed"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13Black}
              date={"12 Sept 2022"}
              status={"Completed"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13Black}
              date={"12 Sept 2022"}
              status={"Completed"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13}
              date={"12 Sept 2022"}
              status={"Pending"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13}
              date={"12 Sept 2022"}
              status={"Pending"}
            />
            <OrderSummary
              name={"iPhone 13"}
              price={"₦730,000.00 x 1"}
              image={Iphone13}
              date={"12 Sept 2022"}
              status={"Pending"}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default DashBoard;
