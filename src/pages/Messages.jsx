import { useEffect, useRef, useState } from "react";
import Search from "../assets/Images/Search.png";
import ChatHead from "../components/messages/chatHead";
import ContactCard from "../components/messages/contactCard";
import Iphone13 from "../assets/Images/Iphone13.png";
import Send from "../assets/Images/Send.png";
import Smile from "../assets/Images/Smile.png";
import Plus from "../assets/Images/Plus.png";
import contact from "../data/contact";
import ChatMessage from "../components/messages/chatMessage";
import chats from "../data/chat";
const Messages = () => {
  const [chat, setChat] = useState(chats)
  const [user, setUser] = useState(contact[0]);
  const [search, setSearch] = useState("");
  const [message, setMessage] = useState("");
  const dummy = useRef(null);
useEffect(() => {
  dummy.current.scrollTop = dummy.current.scrollHeight
}, [chat]);
  const handleSubmit = (e) => {
    e.preventDefault();
    const tempchat = [...chat]
    tempchat.push({
      user: "sender",
      message: message,
      time: "12:55 am",
    });
    tempchat.push({
      user: "receiver",
      message: "Hi, this is a bot message",
      time: "12:57 am",
    });
    setChat(tempchat);
    setMessage("");
  };
  return (
    <div>
      <main className="flex justify-between">
        <h3 className="font-inter font-medium text-base text-customBlack">
          Conversations with Customers
        </h3>
        <button className="text-sm font-inter text-white text-center bg-customBlue hover:bg-customBlue/80  p-2 px-4 rounded-xl">
          New Message
        </button>
      </main>
      <section className="flex flex-col md:flex-row gap-[10px] h-screen mt-5">
        <div className="bg-white w-full md:w-[42%] overflow-y-auto rounded-xl p-4 space-y-4">
          <div className="font-poppins text-xl  font-medium flex justify-between">
            <h3 className="text-customBlack5">Contacts</h3>
            <h3 className="text-customGray3">{contact
              ?.filter((con) =>
                con?.name?.toLowerCase()?.includes(search?.toLowerCase())
              )?.length}</h3>
          </div>
          <div className="w-full gap-4 flex bg-white py-2 px-4 border border-solid border-customHash2 rounded-lg">
            <img alt="search" className="w-5 h-5" src={Search} />
            <input
              className="w-full text-base font-inter bg-transparent outline-none text-customHash3"
              placeholder="Search"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
            />
          </div>
          <div className="py-6 grid grid-cols-1 gap-2">
            {contact
              ?.filter((con) =>
                con?.name?.toLowerCase()?.includes(search?.toLowerCase())
              )
              ?.map((cont, index) => (
                <div onClick={() => setUser(cont)} key={index}>
                  <ContactCard
                    status={cont?.status}
                    message={cont?.message}
                    messageLength={cont?.messageLength}
                    time={cont?.time}
                    picture={cont?.picture}
                    name={cont?.name}
                  />
                </div>
              ))}
          </div>
        </div>
        <div ref={dummy} className="w-full md:w-[58%] h-screen overflow-y-auto  rounded-xl p-4 pb-6 bg-white static">
          <div className="relative">
            <ChatHead
              Picture={user?.picture}
              name={user?.name}
              status={user?.status}
              time={user?.time}
              type={user?.messageLength}
            />
            <div className="mt-2  p-2">
              <div className="flex justify-center">
                <h4 className="text-center bg-customGray5 text-sm font-inter py-2 px-3 rounded-lg mx-auto text-customBlack2 my-2">
                  {" "}
                  12 August 2022{" "}
                </h4>
              </div>
              <div className="w-2/3">
                <section className="p-4 w-full mt-4 space-x-3 flex border-customWhite border border-solid  rounded-2xl text-base font-inter text-white rounded-bl-none">
                  <img
                    src={Iphone13}
                    alt="Iphone 13"
                    className="w-[49px] h-[49px]"
                  />
                  <div className="grid grid-cols-1 content-between w-full">
                    <p className="text-sm font-inter text-customBlack">
                      iPhone 13
                    </p>
                    <div className="flex justify-between items-center w-full">
                      <p className="font-medium text-sm font-inter text-customBlack2">
                        ₦730,000.00
                      </p>
                      <p className="font-inter text-customGray2 text-xs font-medium">
                        <span className="text-customBlue">12 </span> In Stock
                      </p>
                    </div>
                  </div>
                </section>
              </div>
              {chat?.map((cha, index) => (
                <ChatMessage
                  message={cha?.message}
                  user={cha?.user}
                  key={index}
                  date={cha?.date}
                  time={cha?.time}
                />
              ))}
            </div>
            <form
              onSubmit={handleSubmit}
              className="bg-white w-full  p-2 px-4 flex justify-between items-center rounded-lg border border-solid border-customWhite3 fixed bottom-2 z-50 "
            >
              <div className="bg-customYellow6 hover:bg-customYellow6/70 cursor-pointer  relative w-8 h-8 px-4">
                <img
                  src={Plus}
                  alt="Plus"
                  className="w-6 h-6 absolute top-1 left-1"
                />
              </div>
              <input
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                className="outline-none border-none px-4 w-full text-base font-inter text-customHash5"
                placeholder="Your message"
              />

              <img src={Smile} alt="Smile" className="w-6 h-6 cursor-pointer" />
              <button className="mx-4 p-2  w-36  rounded-md bg-customYellow6 hover:bg-customYellow6/70 text-base text-customBlack2">
                Send <img alt="send" src={Send} className="inline w-5 h-5" />
              </button>
            </form>
          </div>
        </div>
      </section>
    </div>
  );
};
export default Messages;
