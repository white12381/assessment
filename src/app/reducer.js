import { createSlice } from '@reduxjs/toolkit'
const initialState = {
 expanded: false
}

export const mainReducer = createSlice({
    name: 'CRM',
    initialState,
    reducers: {
setExpanded: (state, action) => {
    state.expanded = action.payload
}
    }
});
export const { setExpanded  } = mainReducer.actions;
export default mainReducer.reducer;