import { Link, BrowserRouter, Routes, Route } from "react-router-dom";
import DashBoard from "./pages/dashboard";
import Sidebar from "./components/sidebar";
import Navbar from "./components/navbar";
import Messages from "./pages/Messages";
import { useSelector } from "react-redux";

const router = [
  {
    path: "/",
    element: (
      <div className="flex justify-center  items-center h-screen space-x-5">
        <Link
          to={"/dashboard"}
          className="bg-customBlue hover:bg-customBlue/80 text-sm p-3 py-2 rounded-md text-white"
        >
          DashBoard
        </Link>
        <Link
          to={"/messages"}
          className="bg-customBlue hover:bg-customBlue/80 text-sm p-3 py-2 rounded-md text-white"
        >
          Message
        </Link>
      </div>
    ),
  },
  {
    path: "/dashboard",
    element: <DashBoard />,
  },
  {
    path: "/messages",
    element: <Messages />,
  },
];

function App() {
  const expanded = useSelector(state => state.main).expanded;
  return (
    <BrowserRouter className="mx-8 my-2">
      <Navbar />
      <div className="flex">
        <Sidebar />
        <div className={`${expanded ? 'w-3/4' : 'basis-11/12'} transition-all ease-linear duration-500 delay-150  bg-customGray4 h-full pt-6 pb-2 px-4 pl-6`}>
          <Routes>
            {router.map((route, index) => (
              <Route path={route.path} element={route.element} key={index} />
            ))}
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
