import JaneDoe from "../assets/Images/JaneDoe.png"; 
import KunleAdekunle from "../assets/Images/KunleAdekunle.png";
import JanetAdebayo from "../assets/Images/JanetAdebayo.png";
const contact = [
    {
        picture: JaneDoe,
        name: 'Jane Doe',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',
        messageLength: 'New', time: '12:55 am'
    },
    {
        picture: JanetAdebayo,
        name: 'Janet Adebayo',
        message: 'Hello, I want to make enquiries about your product',
        status: 'offline',
        messageLength: 'New', time: '12:55 am'
    },
    {
        picture: KunleAdekunle,
        name: 'Kunle Adekunle',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',
        messageLength: 'New',  time: '12:55 am'
    },
    {
        picture: JaneDoe,
        name: 'Jane Doe',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',
        messageLength: 2,  time: '12:55 am'
    },
    {
        picture: JanetAdebayo,
        name: 'Janet Adebayo',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',  time: '12:55 am'
    },    {
        picture: KunleAdekunle,
        name: 'Kunle Adekunle',
        message: 'Hello, I want to make enquiries about your product',
        status: 'offline',   time: '12:55 am'
    },
    {
        picture: JaneDoe,
        name: 'Jane Doe',
        message: 'Hello, I want to make enquiries about your product',
        status: 'offline',   time: '12:55 am'
    },
    {
        picture: KunleAdekunle,
        name: 'Kunle Adekunle',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',
        messageLength: 2,  time: '12:55 am'
    },    {
        picture: JaneDoe,
        name: 'Jane Doe',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',
        messageLength: 2, time: '12:55 am'
    },
    {
        picture: JanetAdebayo,
        name: 'Janet Adebayo',
        message: 'Hello, I want to make enquiries about your product',
        status: 'offline',
        messageLength: 2, time: '12:55 am'
    },
    {
        picture: KunleAdekunle,
        name: 'Kunle Adekunle',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',
        messageLength: 2,  time: '12:55 am'
    },
    {
        picture: JaneDoe,
        name: 'Jane Doe',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',
        messageLength: 2,  time: '12:55 am'
    },
    {
        picture: JanetAdebayo,
        name: 'Janet Adebayo',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',  time: '12:55 am'
    },    {
        picture: KunleAdekunle,
        name: 'Kunle Adekunle',
        message: 'Hello, I want to make enquiries about your product',
        status: 'offline',   time: '12:55 am'
    },
    {
        picture: JaneDoe,
        name: 'Jane Doe',
        message: 'Hello, I want to make enquiries about your product',
        status: 'offline',   time: '12:55 am'
    },
    {
        picture: KunleAdekunle,
        name: 'Kunle Adekunle',
        message: 'Hello, I want to make enquiries about your product',
        status: 'online',   time: '12:55 am'
    },
]
export default contact;