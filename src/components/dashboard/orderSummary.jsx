const OrderSummary = ({ image, name, price, date, status }) => {
  return (
    <div className="flex justify-between pb-3  border-b-[1px] border-b-solid border-customWhite">
      <div className="flex">
        <img src={image} className="w-[49px] h-[49px]" alt="iphone 13" />
        <section className="grid content-between font-inter text-sm">
          <h5 className="text-customBlack">{name}</h5>
          <p className="text-customBlack4 font-medium">{price}</p>
        </section>
      </div>
      <section className="grid content-between font-inter text-xs">
        <h5 className="text-customGray3">{date}</h5>
        <p
          className={` ${
            status === "Pending"
              ? "bg-customRed2 text-customRed"
              : "bg-customGreen2 text-customGreen"
          } text-center rounded-lg py-[2px] px-[15px]`}
        >
          {status}
        </p>
      </section>
    </div>
  );
};
export default OrderSummary;
