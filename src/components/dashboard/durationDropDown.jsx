import { Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
const DurationDropDown = ({buttonText, buttonStyle, dropDownStyle}) => {
    const items = [
        {
          key: "1",
          label: (
            <a>
              This Month
            </a>
          ),
        },
        {
          key: "2",
          label: (
            <a>
            This Month
            </a>
          ),
        },
        {
          key: "3",
          label: (
            <a>
              This Year
            </a>
          ),
        },
      ];
return <Dropdown
    menu={{
      items,
    }}
  >
    <button className={`text-customGray text-xs font-inter ${buttonStyle}`}>
    {buttonText ? buttonText : 'This Week'} <DownOutlined className={`mx-1 text-customGray ${dropDownStyle}`} />
    </button>
  </Dropdown>
}
export default DurationDropDown;