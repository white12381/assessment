import DurationDropDown from "./durationDropDown";

const SummaryCard = ({
  image,
  contents,
  imageColor,
  customStyle,
  hideDuration,
  titleStyle,
  priceStyle,
  discountStyle,
}) => {
  return (
    <div
      className={`bg-white  rounded-xl py-[11px] space-y-6 px-[15px] ${customStyle}`}
    >
      <div className="flex justify-between">
        <div
          className={`relative w-9 h-9 rounded-lg bg-customBlue2 ${imageColor}`}
        >
          <img
            src={image}
            alt="image"
            className="absolute top-2 left-2 w-5 h-5"
          />
        </div>
        {!hideDuration && <DurationDropDown />}
      </div>
      <div className="flex space-x-6">
        {contents?.map((content, index) => (
          <div key={index} className="space-y-1">
            <h3 className={`font-inter text-sm text-customGray2 ${titleStyle} ${content?.title === "Customers"  && "!text-customGray2" }`}>
              {content?.title}
            </h3>
            <div className="flex flex-col lg:flex-row items-center gap-x-[2px]">
              <p className={`font-poppins font-medium  text-[20px] text-customBlack ${priceStyle}`}>
                {content?.price}{" "}
              </p>
              <p className={`font-inter text-xs ml-[2px] text-customGreen ${discountStyle}`}>
                {content?.discount}
              </p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
export default SummaryCard;
