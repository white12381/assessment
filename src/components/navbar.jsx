import Logo from "../assets/Images/logo.png";
import Notification from "../assets/Images/Notification.png";
import Home from "../assets/Images/Home.png";
import Profile from "../assets/Images/profile.png";
import { Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
import { Link, useLocation } from "react-router-dom";
import {useDispatch, useSelector} from "react-redux"
import { useEffect, useState } from "react";
import { setExpanded } from "../app/reducer";
const Navbar = () => {
  const location = useLocation();
  const { pathname } = location;
  const items = [
    {
      key: "1",
      label: <a>1st menu item</a>,
    },
    {
      key: "2",
      label: <a>2nd menu item</a>,
    },
    {
      key: "3",
      label: <a>3rd menu item</a>,
    },
  ];
  const [title, setTitle] = useState("");
  useEffect(() => {
    if (pathname === "/dashboard") {
      setTitle("DashBoard");
    } else if (pathname === "/messages") {
      setTitle("Conversations");
    } else {
      setTitle("Home");
    }
  }, [pathname]);
  const dispatch = useDispatch();
  const expanded = useSelector(state => state.main).expanded;
  return (
    <nav className="flex pl-8 pr-2">
      <aside onClick={() => dispatch(setExpanded(!expanded))} className="flex cursor-pointer basis-1/4 items-center bg-white">
        <img src={Logo} alt="logo" className="w-[52px] h-[52px]" />
        <h1 className="font-poppins text-xl font-bold text-customBlack">
          Metrix
        </h1>
      </aside>
      <section className="basis-3/4 bg-white p-4 space-y-4">
        <main className="flex justify-between">
          <h1 className="font-poppins font-medium text-xl text-customBlack">
            {title}
          </h1>
          <div className="flex items-center gap-x-4">
            <Dropdown
              menu={{
                items,
              }}
            >
              <button className="rounded-lg text-inter font-normal text-sm text-customBlack2 bg-customYellow p-1 px-3">
                Nanny’s Shop <DownOutlined className="mx-1" />
              </button>
            </Dropdown>
            <img
              src={Notification}
              alt="Notification"
              className="h-5 w-5 cursor-pointer"
            />
            <img src={Profile} alt="Profile" className="h-8 w-8" />
          </div>
        </main>
        <div className="border-customWhite border-[0px] border-t-[1px] border-solid py-1 px-5 ">
          <Link to={"/"}>
            <img src={Home} alt="Home" className="h-4 w-4 inline" />{" "}
          </Link>{" "}
          <p className="text-xs font-inter text-customGray2 inline mx-3">/</p>
          <p className="text-xs font-inter text-customGray2 inline">{title !== "Home" && title}</p>
        </div>
      </section>
    </nav>
  );
};
export default Navbar;
