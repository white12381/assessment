import Category from "../assets/Images/Category.png";
import Folder from "../assets/Images/Folder.png";
import User from "../assets/Images/User.png";
import Bag from "../assets/Images/Bag.png";
import Chat from "../assets/Images/Chat.png";
import Setting from "../assets/Images/Setting.png";
import HeadPhone from "../assets/Images/Headphones.png";
import Logout from "../assets/Images/Logout.png";
import Gift from "../assets/Images/Gift.png";
import { Link, useLocation } from "react-router-dom";
import CategoryLight from "../assets/Images/CategoryLight.png";
import {  useSelector } from "react-redux"; 
const Sidebar = () => {
  const location = useLocation();
  const { pathname } = location; 
  const expanded = useSelector(state => state.main).expanded;
  return (
    <div className={`bg-white  transition-all ease-linear duration-500 delay-150  grid content-between ${expanded ? 'basis-1/4 ml-8' : 'basis-1/12 ml-4'}`}>
      <nav className="space-y-2 mt-4"> 
        <Link  to={'/dashboard'}
          className={`sideBarLink !text-customBlack3 hover:bg-customBlue hover:!text-white ${
            pathname === "/dashboard" && "bg-customBlue !text-white"
          }`}
        >
          <img src={`${pathname === "/dashboard" ? Category : CategoryLight}`} alt="Category" /> {expanded && <h2> Dashboard </h2>}{" "}
        </Link> 
        <a className="sideBarLink !text-customBlack3 hover:bg-customBlue hover:!text-white">
          <img src={Bag} alt="Bag" />{ expanded && <h2> Orders </h2> }<div>3</div>
        </a>
        <a className="sideBarLink !text-customBlack3 hover:bg-customBlue hover:!text-white">
          <img src={User} alt="User" />{expanded && <h2> Customers </h2>}{" "}
        </a>
        <a className="sideBarLink !text-customBlack3 hover:bg-customBlue hover:!text-white">
          <img src={Folder} alt="Folder" /> {expanded && <h2> Inventory </h2>}{" "}
        </a>
        <Link to={'/messages'} className={`sideBarLink !text-customBlack3 hover:bg-customBlue hover:!text-white
        ${
          pathname === "/messages" && "bg-customBlue !text-white"
        }`}>
          <img src={Chat} alt="Chat" /> {expanded && <h2> Conversations </h2> }<div>16</div>
        </Link>
        <a className="sideBarLink !text-customBlack3 hover:bg-customBlue hover:!text-white">
          <img src={Setting} alt="Setting" /> {expanded && <h2> Settings </h2>}{" "}
        </a>
      </nav>
      <nav className="space-y-6">
        <div className="space-y-4">
          <a className="sideBarLink !rounded-2xl !text-customBlack3 bg-[#5E63661A]">
            <img src={HeadPhone} alt="HeadPhone" /> { expanded && <h2> Contact Support </h2>}{" "}
          </a>
          <div className="bg-customYellow3 rounded-2xl w-2/3 p-2">
            <a className="sideBarLink !cursor-none !w-full !text-customBlack3 bg-transparent !gap-x-2 !px-0">
              <img src={Gift} alt="Gift" /> { expanded && <h2> Free Gift Awaits You! </h2>}{" "}
            </a>
          { expanded &&  <a href="#" className="text-[#6E7079] text-xs cursor-pointer">
              Upgrade your account <span className="ml-2">{">"} </span>
            </a> }
          </div>
        </div>
        <a className="sideBarLink !rounded-2xl !text-customRed !text-xs items-center">
          <img src={Logout} alt="Logout" /> {expanded && <h2> Logout </h2>}{" "}
        </a>
      </nav>
    </div>
  );
};
export default Sidebar;
