
import Check from "../../assets/Images/Check.png";
const ChatMessage = ({time, date, message, user}) => {
    return <div>
        { date && <div className="flex justify-center">
        <h4 className="text-center bg-customGray5 text-sm font-inter    py-2 px-3 rounded-lg mx-auto text-customBlack2 my-4">
              {date}
            </h4>
            </div>
}
            { user === 'receiver' &&
            <div className="w-2/3">
            <p className="p-4 mt-4  rounded-2xl text-base font-inter text-white rounded-bl-none bg-customBlue">
                {message}
              </p>
              <p className="text-base font-inter text-customGray2 mt-2">
                {time}
              </p>
            </div>
}
            {user === 'sender' && <div className="flex justify-end">
            <div className="w-2/3">
              <p className=" p-4 mt-4  rounded-2xl text-base font-inter text-customBlack2 rounded-bl-none bg-customYellow5">
                {message}
              </p>
              <div className="justify-end space-x-2 flex items-center mt-2">
                <p className="text-base font-inter text-customGray2">
                  {time}
                </p>
                <span className="bg-customYellow5 w-5 h-5 relative rounded-full px-1">
                  <img
                    src={Check}
                    className="absolute top-1 left-1 w-3 h-3"
                    alt="check"
                  />
                </span>
              </div>
            </div>
            </div>
}
    </div>
}
export default ChatMessage