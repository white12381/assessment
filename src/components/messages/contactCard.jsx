const ContactCard = ({
  picture,
  name,
  message,
  status,
  messageLength,
  time,
}) => {
  return (
    <section className="bg-white flex justify-start gap-x-[14px] rounded-md cursor-pointer hover:bg-customGray4 p-4">
      <div className="w-1/6">
        <div className="h-12 w-12 border border-solid rounded-lg relative border-customWhite">
          <img src={picture} alt={name} className="h-full w-full" />
          <div
            className={`${
              status === "online" ? "bg-customBlue4" : "bg-customHash4"
            } w-2 h-2 absolute top-1 left-[41px] rounded-full outline outline-customWhite2 outline-2`}
          ></div>
        </div>
      </div>
      <div className="w-5/6 grid grid-cols-1 content-between">
        <div className="font-inter flex w-full justify-between items-start">
          <h3 className="text-base font-medium text-customBlack">{name}</h3>
          { messageLength === 'New' &&
          <h4 className="bg-customYellow rounded-md py-[2px] px-[7px] text-xs">
            {messageLength}
          </h4>}
          { parseInt(messageLength) >= 0 &&
          <h4 className="bg-customYellow2 rounded-full  py-[2px] px-[7px] text-xs">
            {messageLength}
          </h4>
}
        </div>
        <div className="flex justify-between items-center font-inter text-customGray2 ">
          <h6 className=" text-sm truncate w-3/5">{message}</h6>
          <h6 className="text-xs ">{time}</h6>
        </div>
      </div>
    </section>
  );
};
export default ContactCard;
