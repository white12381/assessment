// import JaneDoe from "../assets/Images/JaneDoe.png";
// import Bag from "../assets/Images/Bag.png";
import Bag from "../../assets/Images/Bag.png"; 
const ChatHead = ({ Picture, status, time, type, name }) => {
  return (
    <div className=" flex justify-between p-3 border-customWhite border-[0px] border-b-[1px] border-solid">
      <div className="flex space-x-4">
        <div className="h-14 w-14 border border-solid rounded-lg  border-customWhite">
          <img src={Picture} alt="fineGirl1" className="h-full w-full" />
        </div>
        <div className="grid grid-col content-between">
          <h4 className="font-inter font-medium text-base text-customBlack">
            {name}
          </h4>
          <p>
            <span
              className={`w-2 h-2  inline-block rounded-full 
          outline outline-customWhite2 outline-2 ${
            status === "online" ? "bg-customBlue4" : "bg-customHash4"
          }`}
            ></span>
            <span className="font-inter text-sm text-customBlue5 mx-2">
              {status}
            </span>
            <span className="text-sm font-inter text-customGray2">{time}</span>
          </p>
        </div>
      </div>
      <div className="grid grid-col content-between">
        <div className="flex justify-end items-center space-x-2">
         {type === 'New' && <h4 className="rounded-lg text-inter font-normal text-sm text-customBlack2 bg-customYellow py-[2px] px-[7px]">
            New Customer
          </h4>
}
          <p className="text-sm font-inter font-normal text-customBlue4 cursor-pointer">
            View Profile
          </p>
        </div>
        <p className="flex justify-end text-sm text-customGray3">
          <img alt="bag" src={Bag} className="w-5 h-5 mx-2" />0 Orders
        </p>
      </div>
    </div>
  );
};
export default ChatHead;
